var Account = function (saldo, waluta) {
    this.saldo = saldo;
    this.waluta = waluta;
};
var person = (function () {
    var details = {
        firstName: 'Marian',
        lastName: 'Pazdzioch',
    };
    accountsList = []
    calculateBalance = function () {
        var suma = 0;
        for (var i = 0; i < accountsList.length; i++) {
            suma += accountsList[i].saldo;
        }
        return suma;
    }
    return {
        imie: details.firstName,
        nazwisko: details.lastName,
        sayHello: function () {
            return 'Imię: ' + this.imie + ', Nazwisko: ' + this.nazwisko + ', liczba kont: ' + accountsList.length + ', suma: ' +
                calculateBalance();
        },
        addAccount: function (saldo, waluta) {
                accountsList.push(new Account (saldo, waluta));
            }
        }
    })();
console.log(person.sayHello());
person.addAccount(120408, 'franki');
console.log(person.sayHello());