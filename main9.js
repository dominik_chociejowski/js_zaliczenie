const init = (() => {
    var person = function () {
        var details = {
            firstName: 'Baba',
            lastName: 'Chytra',
            accountsList: [
                {
                    numer: 1,
                    saldo: 696969,
                    waluta: 'jeny'
                },
                {
                    numer: 2,
                    saldo: 654321,
                    waluta: 'liry'
                }
            ]
        };
        return {
            imie: details.firstName,
            nazwisko: details.lastName,
            account: details.accountsList,

            findAccount: function (numer) {
                return details.accountsList.find((objekt) => objekt.numer === numer);
            },

            withdraw: function (numer, kwota) {
                return new Promise((resolve, reject) => {
                    const chooseAccount = this.findAccount(numer);
                    if (chooseAccount && chooseAccount.saldo >= kwota && kwota > 0) {
                        const wyliczone = chooseAccount.saldo - kwota;
                        setTimeout(() => resolve(`Numer konta: ${chooseAccount.numer}, Wypłata: ${kwota}, Saldo po wypłacie: ${chooseAccount.saldo = wyliczone} `), 3000);
                    }
                    else if (kwota <= 0) {
                        reject('Nie można wypłacić ujemnej kwoty');
                    }
                    else if (!chooseAccount) {
                        reject('Nie ma konta o takim numerze');
                    }
                    else {
                        reject('Brak środków');
                    }
                })
            }
        }
    };
    let cardTitle = document.querySelector("h4.card-title");
    let cardText = document.querySelector("p.card-text");
    let messageBox = document.querySelector("div#message");
    let fieldNumber = document.querySelector("#number");
    let fieldAmount = document.querySelector("#amount");
    let buttonSubmit = document.querySelector("button.btn.btn-primary");

    let showAccInHTML = function (nameOfAcc) {
        cardText.innerHTML = 'Accounts:';
        for (i = 0; i < nameOfAcc.account.length; i++) {
            let accountsListString = `Numer konta: ${nameOfAcc.account[i].numer}, Saldo: ${nameOfAcc.account[i].saldo},Waluta: ${nameOfAcc.account[i].waluta} `;
            cardText.innerHTML += "<p>" + accountsListString + "</p>";
        }
    };

    const somePerson = person();
    cardTitle.innerHTML = `${somePerson.imie} ${somePerson.nazwisko}`;
    showAccInHTML(somePerson);


    return {
        withdrawHTML: function () {
            let accountNumber = parseInt(document.getElementById('number').value);
            let accountAmount = parseInt(document.getElementById('amount').value);

            somePerson.withdraw(accountNumber, accountAmount)
                .then((msg) => console.log(messageBox.innerText = msg))
                .then(() => showAccInHTML(somePerson))
                .catch((err) => console.log(messageBox.innerText = err));
        },
        onChanged: function () {
                buttonSubmit.disabled = fieldNumber.value.length === 0 || fieldAmount.value.length === 0;
        }
    }
})();