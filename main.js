function personFactory() {
    var details = {
        firstName: 'Marian',
        lastName: 'Pazdzioch',
        accountsList: [
            {
                saldo: 1234,
                waluta: 'zloty'
            },
            {
                saldo: 5462,
                waluta: 'dolar'
            },
            {
                saldo: 987546,
                waluta: 'euro'
            }
        ]
    };
    return {
        imie: details.firstName,
        nazwisko: details.lastName,
        sayHello: function () {
            return 'Imię: ' + this.imie + ', Nazwisko: ' + this.nazwisko + ', liczba kont: ' + details.accountsList.length
        }
    }
}
var zmienna = personFactory();
console.log(zmienna.sayHello());