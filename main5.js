var Account = function (saldo, waluta, numer) {
    this.saldo = saldo;
    this.waluta = waluta;
    this.numer = numer;
};
var person = (function () {
    var details = {
        firstName: 'Marian',
        lastName: 'Pazdzioch',
    };
    accountsList = [];
    calculateBalance = function () {
        var suma = 0;
        for (var i = 0; i < accountsList.length; i++) {
            suma += accountsList[i].saldo;
        }
        return suma;
    };
    return {
        imie: details.firstName,
        nazwisko: details.lastName,
        sayHello: function () {
            return 'Imię: ' + this.imie + ', Nazwisko: ' + this.nazwisko + ', liczba kont: ' + accountsList.length + ', suma: ' + calculateBalance();
        },
        addAccount: function (saldo, waluta, numer) {
            accountsList.push(new Account(saldo, waluta, numer));
        },
        findAccount: function (numer) {
            return accountsList.find((objekt) => objekt.numer === numer);
        },
        withdraw: function (numer, kwota) {
            return new Promise((resolve, reject) => {
                const znajdzKonto = this.findAccount(numer);
                if (znajdzKonto && znajdzKonto.saldo >= kwota) {
                    const wyliczone = znajdzKonto.saldo - kwota;
                    setTimeout (function() {resolve(`Numer konta: ${znajdzKonto.numer}, Wypłata: ${kwota}, Stan konta po wypłacie: ${znajdzKonto.saldo = wyliczone} `)}, 3000);
                }
                else if (!znajdzKonto) {
                    reject('Nie ma konta o takim numerze');
                }
                else {
                    reject('Brak środków');
                }
            })
    
        }
    }
})();
console.log(person.sayHello());
person.addAccount(120408, 'franki', 1);
console.log(person.sayHello());
console.log(person.findAccount(1));
person.withdraw(1, 323)
    .then((success) => console.log(success))
    .catch((err) => console.log(err));