var person = (function () {
    var details = {
        firstName: 'Marian',
        lastName: 'Pazdzioch',
    };
    calculateBalance = function () {
        var suma = 0;
        for (i = 0; i < person.accountsList.length; i++) {
            suma += person.accountsList[i].saldo;
        }
        return suma;
    }
    return {
        accountsList: [
            {
                saldo: 1234,
                waluta: 'zloty'
            },
            {
                saldo: 5462,
                waluta: 'dolar'
            },
            {
                saldo: 987546,
                waluta: 'euro'
            }
        ],
        imie: details.firstName,
        nazwisko: details.lastName,
        sayHello: function () {
            return 'Imię: ' + this.imie + ', Nazwisko: ' + this.nazwisko + ', liczba kont: ' + person.accountsList.length + ', suma: ' +
                calculateBalance();
        },
        addAccount: function (saldo, waluta) {
            
            person.accountsList.push({
                saldo: saldo,
                waluta: waluta
            });
        }
    }
}
)();
console.log(person.sayHello());
person.addAccount(120408, 'franki');
console.log(person.sayHello());